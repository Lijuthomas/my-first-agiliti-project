import React from "react";

const Result = ({score, playAgain}) => (
    <div className="score-board">
        <div className="score"> Your score is {score} out of 5 correct answers!</div>
        <button className="playBtn" onClick={playAgain}>
            Load Next Set of Questions!
        </button>
    </div>
);

export default Result;