const qBank = [
  {
    question:
      "Everything in React is a _____________ ",
    answers: ["Module", "Component", "Package", "Class"],
    correct: "Component",
    questionId: "099099"
  },
  {
    question:
      'In which directory React Components are saved?',
    answers: ["Inside vendor/", "Inside external/components/", "Inside vendor/components/", "Inside js/components/"],
    correct: "Inside js/components/",
    questionId: "183452"
  },
  {
    question:
      "How many elements does a react component return?",
    answers: ["2 Elements", "Multiple Elements", "None of These", "1 Elements"],
    correct: "Multiple Elements",
    questionId: "267908"
  },
  {
    question: "What is state in React?",
    answers: [
      "A persistant storage.",
      "An internal data store (object) of a component." ],
    correct: "An internal data store (object) of a component.",
    questionId: "333247"
  },
  {
    question: "What is Babel?",
    answers: ["A Compiler", "An interpreter", "Both Compiler and Transpilar", "A transpiler."],
    correct: "Both Compiler and Transpilar",
    questionId: "496293"
  },
  {
    question:
      "What does the 'webpack' command do?",
    answers: [
      "A module bundler",
      "Runs react local development server.",
      "The Bushwhackers"
    ],
    correct: "A module bundler",
    questionId: "588909"
  },
  {
    question: "What port is the default where the webpack-dev-server will run?",
    answers: ["8080", "3000", "3306"],
    correct: "8080",
    questionId: "648452"
  },
  {
    question: "What is ReactJS?",
    answers: ["Server side Framework", "A Library for building interaction interfaces", "User-interface framework", "None of These"],
    correct: "A Library for building interaction interfaces",
    questionId: "786649"
  },
  {
    question:
      "What are the two ways that data gets handled in React?",
    answers: ["state & props", "services & components"],
    correct: "state & props",
    questionId: "839754"
  },
  {
    question:
      "In React what is used to pass data to a component from outside?",
    answers: [
      "setState",
      "render with arguments",
      "PropTypes",
      " props"
    ],
    correct: " props",
    questionId: "98390"
  },
  {
    question: "How can you access the state of a component from inside of a member function?",
    answers: ["this.getState()", "this.values", "this.prototype.stateValue", "this.state"],
    correct: "this.values",
    questionId: "1071006"
  },
  {
    question: "Props are __________ into other components",
    answers: ["Methods", "Injected", "Both 1 & 2", "All of the above"],
    correct: "Methods",
    questionId: "1174154"
  },
  {
    question: "What is a react.js in MVC?",
    answers: ["Middleware", "Controller", "Model", "Router"],
    correct: "Controller",
    questionId: "1226535"
  },
  {
    question: "ReactJS uses _____ to increase performance",
    answers: ["Original DOM", " None of above", "Both 1 & 2", "Virtual DOM"],
    correct: "Virtual DOM",
    questionId: "1310938"
  },
  {
    question: "Keys are given to a list of elements in react. These keys should be -",
    answers: ["Do not requires to be unique", "Unique among the siblings only", "Unique in the DOM", "All of the above"],
    correct: "Unique among the siblings only",
    questionId: "1436873"
  },
  {
    question: "Which of the following is the correct data flow sequence of flux concept?",
    answers: ["Dispatcher->Action->Store->View", "Action->Dispatcher->View->Store", "Action->Dispatcher->Store->View", "Action->Store->Dispatcher->View"],
    correct: "Action->Dispatcher->Store->View",
    questionId: "1515110"
  },
  {
    question: "React.js Covers only the view layer of the app.",
    answers: ["Yes", "No"],
    correct: "Yes",
    questionId: "1642728"
  },
  {
    question:
      "What is the name of React.js Developer ?",
    answers: ["Jordan Walke", "Tim Lee", "Jordan Lee", "Jordan mike"],
    correct: "Jordan Walke",
    questionId: "1747256"
  },
  {
    question:
      "Who Develop React.js?",
    answers: ["Apple", "Facebook", "Twitter", "Google"],
    correct: "Facebook",
    questionId: "1822532"
  },
  {
    question: ".............. helps react for keeping their data unidirectional.",
    answers: [
      "JSX",
      "Flux",
      "Dom",
      "Props"
    ],
    correct: "Flux",
    questionId: "195075"
  },
  {
    question: 'The state in react can be updated by call to setState method. These calls are',
    answers: ["Synchronous in nature", "Asynchronous in nature", "Are asynchronous but can be made synchronous when required", "None of above"],
    correct: "Asynchronous in nature",
    questionId: "2019778"
  },
  {
    question: "As soon as the state of react component is changed, component will",
    answers: ["Does nothing , you have to call render method to render the component again", "re-renders the component", "be created again from scratch", "None of above"],
    correct: "re-renders the component",
    questionId: "2134343"
  },
  {
    question: "React Lifecycle method static getDerivedSateFromProps(props , state) is called when ____",
    answers: [
      "Component is created for the first time",
      "State of the component is updated",
      "Component is created for the first time and State of the component is updated",
      "None of above"
    ],
    correct: "Component is created for the first time and State of the component is updated",
    questionId: "2210799"
  },

  {
    question: "componentDidMount lifecycle method is called when____",
    answers: ["Component is created for the first time", "Component is updated", "Both of above", "None of above"],
    correct: "Component is created for the first time",
    questionId: "2426418"
  },
  {
    question: "Keys are given to a list of elements in react. These keys should be ",
    answers: ["Unique in the DOM", "Unique among the siblings only", "Do not requires to be unique", "None of above"],
    correct: "Unique among the siblings only",
    questionId: "2510086"
  },
  {
    question: "For controlled components in react",
    answers: [
      "Source of truth is DOM",
      "Source of truth is component state",
      "Source of truth can be anything",
      "None of above"
    ],
    correct: "Source of truth is component state",
    questionId: "2685745"
  },
  {
    question:
      "For uncontrolled components in react",
    answers: ["Source of truth is DOM", "Source of truth is component state", "Source of truth can be anything", "None of above."],
    correct: "Source of truth is DOM",
    questionId: "2796884"
  },
  {
    question: "o upload a file from react component, A developer will require to write",
    answers: ["A controlled component", "An uncontrolled component", "Can be done from both", "None of above"],
    correct: "An uncontrolled component",
    questionId: "2838900"
  },
  {
    question: 'Everything in react is __________',
    answers: ["module", "component", " package", " class"],
    correct: "component",
    questionId: "298060"
  },
  {
    question: " Props are __________ into other components",
    answers: ["Injected", "Methods", "Both A and B", "None"],
    correct: "Methods",
    questionId: "3096579"
  },
  {
    question: "Which of the following API is a MUST for every ReactJS component?",
    answers: [
      "getInitialState",
      " render",
      "renderComponent",
      "None"
    ],
    correct: "renderComponent",
    questionId: "3182461"
  },
  {
    question: "At the highest level, React components have lifecycle events that fall into",
    answers: [
      "Initialization",
      "State/Property Updates",
      "Destruction",
      "All"
    ],
    correct: "All",
    questionId: "3239112"
  }
];

export default (n = 5) =>

  Promise.resolve(qBank.sort(() => 0.5 - Math.random()).slice(0, n));
